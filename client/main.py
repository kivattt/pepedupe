import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serverAddressFile = open("serveraddress.txt", "r")
contents = serverAddressFile.read().split()
serverIP, serverPort, password = contents[0], int(contents[1]), contents[2]

print("Connecting to " + serverIP + ':' + str(serverPort))
print("Password: " + password)

try:
	sock.connect((serverIP, serverPort))
except:
	print("Error connecting to server")
	sys.exit(1)

print("Connected to server")

exit = False
while not exit:
	link = input("Enter link: ")

	try:
		sock.sendall(bytes(password + link, "ascii"))
	except IOError as e:
		print("Error sending link")
sock.close()
