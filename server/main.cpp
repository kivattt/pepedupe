#include <iostream>
#include <cstring>
#include <list>
#include <thread>
#include <vector>
#include <string>
#include <SFML/Network.hpp>

using std::vector;
using std::string;

const unsigned short port = 61432;

int main(){
	sf::TcpListener listener;
	if (listener.listen(port) != sf::Socket::Done){
		std::cout << "Error: could not listen on port " << port << ", exiting...\n";
		return 1;
	}

	std::list <sf::TcpSocket*> clients;

	sf::SocketSelector selector;
	selector.add(listener);

	while (true){
		if (selector.wait()){
			if (selector.isReady(listener)){
				sf::TcpSocket* client = new sf::TcpSocket;
				if (listener.accept(*client) == sf::Socket::Done){
					std::cout << client->getRemoteAddress() << " connected\n";
					clients.push_back(client);
					selector.add(*client);
				} else{
					delete client;
				}
			} else{
				for (std::list <sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); ++it){
					sf::TcpSocket& client = **it;
					if (selector.isReady(client)){
						char data[1024];
						memset(data, 0, sizeof(data));
						std::size_t received = 0;
						auto statusCode = client.receive(data, sizeof(data), received);
						if (statusCode == sf::Socket::Done){
							std::cout << client.getRemoteAddress() << ' ' << string(data) << '\n';
						} else if (statusCode == sf::Socket::Disconnected){
							std::cout << client.getRemoteAddress() << " disconnected\n";
							clients.erase(it--);
							selector.remove(client);
						}
					}
				}
			}
		}
	}
}
